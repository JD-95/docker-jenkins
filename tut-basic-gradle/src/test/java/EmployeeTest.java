import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void createEmployeeSuccessfully() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act
        Employee employee = new Employee(firstName, lastName, description, jobTitle, email);
        //assert
        assertNotNull(employee);
    }

    @Test
    void failureCreateEmployee_FirstNameNull() {
        //arrange
        String firstName = null;
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_FirstNameEmpty() {
        //arrange
        String firstName = "";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_FirstNameBlank() {
        //arrange
        String firstName = "   ";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_LastNameNull() {
        //arrange
        String firstName = "Constantino";
        String lastName = null;
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_LastNameEmpty() {
        //arrange
        String firstName = "Constantino";
        String lastName = "";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_LastNameBlank() {
        //arrange
        String firstName = "Constantino";
        String lastName = "    ";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_DescriptionNull() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = null;
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_DescriptionEmpty() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_DescriptionBlank() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "   ";
        String jobTitle = "boss";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_JobTitleNull() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = null;
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_JobTitleEmpty() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_JobTitleBlank() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "   ";
        String email = "boss@gmail.com";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_EmailNull() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = null;
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_EmailEmpty() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @Test
    void failureCreateEmployee_EmailBlank() {
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        String email = "  ";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }

    @ParameterizedTest
    @ValueSource(strings = {"newEmail@.com", "newEmail@gmail", "@gmail.com", "ne@Test@gmail.com", " newTest@gmail.com ", "new&Test@gmail.com"})
    void failureCreateEmployee_InvalidEmail(String email){
        //arrange
        String firstName = "Constantino";
        String lastName = "Pacheco";
        String description = "cat-lover";
        String jobTitle = "boss";
        //act - assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, jobTitle, email));
    }
}
